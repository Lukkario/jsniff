package jsnif.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import jsnif.controller.SnifferController;
import jsnif.model.PacketData;

import java.util.Set;


public class MainWindowView {

    private static ObservableList<PacketData> data = null;
    private static SnifferController controller;

    public static void setData(Set<PacketData> packets) {
        data = FXCollections.observableArrayList(packets);
    }

    public static ObservableList<PacketData> getData()
    {
        return data;
    }

    @FXML
    TableView DATATABLE;

    @FXML
    TableColumn CLIENTMAC_COLUMN;

    @FXML
    TableColumn BSSID_COLUMN;

    @FXML
    TableColumn TIME_COLUMN;

    @FXML
    TableColumn SSID_COLUMN;

    @FXML
    MenuItem NIC_SELECT_MENU_ITEM;

    @FXML
    MenuItem START_MENU_ITEM;

    @FXML
    MenuItem STOP_MENU_ITEM;

    @FXML
    public void initialize()
    {
        controller.setMainWindowView(this);

        CLIENTMAC_COLUMN.setCellValueFactory(new PropertyValueFactory<>("CLIENT_MAC"));
        BSSID_COLUMN.setCellValueFactory(new PropertyValueFactory<>("BSSID"));
        TIME_COLUMN.setCellValueFactory(new PropertyValueFactory<>("TIME"));
        SSID_COLUMN.setCellValueFactory(new PropertyValueFactory<>("SSID"));
        DATATABLE.setItems(data);
    }

    public static void setController(SnifferController snifferController)
    {
        controller = snifferController;
    }

    public void showNicView()
    {
        controller.showNicView();
    }

    public void startSniffing()
    {
        if (controller.getNIC() != null)
        {
            controller.start();
        }
    }

    public void stopSniffing()
    {
        controller.stopSnifferThread();
    }

    public void enableStart()
    {
        START_MENU_ITEM.setDisable(false);
    }

}
