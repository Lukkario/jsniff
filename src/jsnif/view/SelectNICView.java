package jsnif.view;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import jsnif.controller.SnifferController;

public class SelectNICView {

    private String nic = "";
    private static SnifferController controller;

    @FXML
    Button NIC_OK;

    @FXML
    TextField NIC_NAME;

    @FXML
    AnchorPane NIC_CHOOSE_PANE;

    @FXML
    public void initialize()
    {

    }

    public static void setController(SnifferController snifferController)
    {
        controller = snifferController;
    }

    public void submit()
    {
        nic = NIC_NAME.getText();
        System.out.println(nic);
        controller.setNIC(nic);
        close();
    }

    public void close()
    {
        Stage stage = (Stage) NIC_CHOOSE_PANE.getScene().getWindow();
        if(!nic.equals(""))
        {
            controller.enableStart();
        }
        stage.close();
    }

}
