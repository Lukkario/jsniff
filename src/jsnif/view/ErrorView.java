package jsnif.view;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import jsnif.controller.SnifferController;

public class ErrorView {

    private static SnifferController controller;

    @FXML
    AnchorPane ERROR_PANE;

    @FXML
    TextArea ERROR_MSG;

    public static void setController(SnifferController snifferController)
    {
        controller = snifferController;
    }

    public void close()
    {
        Stage stage = (Stage) ERROR_PANE.getScene().getWindow();
        stage.close();
    }

    public void initialize()
    {
        ERROR_MSG.setText(controller.getErrorMessage());
    }

}
