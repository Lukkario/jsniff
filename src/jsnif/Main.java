package jsnif;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import jsnif.controller.SnifferController;

public class Main extends Application {

    private SnifferController snifferController;

    @Override
    public void start(Stage primaryStage) throws Exception{
        snifferController = new SnifferController();

        Parent root = FXMLLoader.load(getClass().getResource("res/MainWindow.fxml"));
        primaryStage.setTitle("JSnif");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();


    }

    @Override
    public void stop() throws Exception
    {
        super.stop();
        snifferController.stopSnifferThread();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
