package jsnif.model;

import javafx.collections.ObservableList;
import jsnif.controller.SnifferController;
import org.pcap4j.core.PcapHandle;
import org.pcap4j.core.PcapNativeException;
import org.pcap4j.packet.Dot11ProbeRequestPacket;
import org.pcap4j.packet.Packet;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static java.lang.Thread.sleep;

public class SnifferThreadModel implements Runnable {

    private ObservableList<PacketData> packets = null;
    private PcapHandle handle = null;
    private Boolean run = true;
    private SnifferController controller;

    public SnifferThreadModel(ObservableList<PacketData> packets, PcapHandle handle, SnifferController controller)
    {
        this.packets = packets;
        this.handle = handle;
        this.controller = controller;
    }

    public void stopThread()
    {
        run = false;
    }


    @Override
    public void run() {
        try {
            Packet packet;
            PacketData packetData;
            Dot11ProbeRequestPacket req;
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss dd/MM/YYYY");

            while (run) {
                packet = handle.getNextPacketEx();

                if ((req = packet.get(Dot11ProbeRequestPacket.class)) != null) {
                    synchronized(packets)
                    {
                        packetData = new PacketData(req.getHeader().getAddress2().toString(), req.getHeader().getAddress3().toString(), dtf.format(LocalDateTime.now()), req.getHeader().getSsid().getSsid());
                        if(!packets.contains(packetData))
                            packets.add(packetData);
                    }
                }
                sleep(10);
            }
        }
        catch (PcapNativeException e)
        {
            handle.close();
            controller.restartSniffingThread();
        }
        catch (Exception e)
        {
            handle.close();
            e.printStackTrace();
        }
        finally {
            handle.close();
        }
    }
}
