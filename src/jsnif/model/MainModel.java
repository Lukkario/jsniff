package jsnif.model;

import org.pcap4j.core.PcapHandle;
import org.pcap4j.core.PcapNativeException;
import org.pcap4j.core.PcapNetworkInterface;
import org.pcap4j.core.Pcaps;

import java.util.LinkedHashSet;
import java.util.Set;

public class MainModel {

    private static final Set<PacketData> packets = new LinkedHashSet<>();
    private final Integer snapLen = 65536;
    private final Integer timeout = 10;
    private final PcapNetworkInterface.PromiscuousMode mode = PcapNetworkInterface.PromiscuousMode.PROMISCUOUS;
    private PcapNetworkInterface nic = null;
    private PcapHandle handle = null;

    public MainModel()
    {
        super();
    }

    public Set<PacketData> getPackets() {
        return packets;
    }

    public void addPacket(PacketData packet)
    {
        packets.add(packet);
    }

    public boolean setUpNetworkInterface(String nicName)
    {
        try {
            if((this.nic = Pcaps.getDevByName(nicName)) == null)
                return false;
            return true;
        } catch (PcapNativeException e) {
            return false;
        }
    }

    public void openHandle() throws PcapNativeException {
        this.handle = this.nic.openLive(snapLen, mode, timeout);
    }

    public PcapHandle getHandle() {
        return handle;
    }
}
