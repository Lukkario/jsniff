package jsnif.model;

import javafx.beans.property.SimpleStringProperty;

import java.io.Serializable;

public class PacketData implements Serializable {
    private SimpleStringProperty CLIENT_MAC;
    private SimpleStringProperty BSSID;
    private SimpleStringProperty TIME;
    private SimpleStringProperty SSID;

    public PacketData(String CLIENT_MAC, String BSSID, String TIME, String SSID) {
        this.CLIENT_MAC = new SimpleStringProperty(CLIENT_MAC);
        this.BSSID = new SimpleStringProperty(BSSID);
        this.TIME = new SimpleStringProperty(TIME);
        this.SSID = new SimpleStringProperty(SSID);
    }

    public PacketData()
    {
        super();
    }

    public String getCLIENT_MAC() {
        return CLIENT_MAC.get();
    }

    public void setCLIENT_MAC(String CLIENT_MAC) {
        this.CLIENT_MAC = new SimpleStringProperty(CLIENT_MAC);;
    }

    public String getBSSID() {
        return BSSID.get();
    }

    public void setBSSID(String BSSID) {
        this.BSSID = new SimpleStringProperty(BSSID);
    }

    public String getTIME() {
        return TIME.get();
    }

    public void setTIME(String TIME) {
        this.TIME = new SimpleStringProperty(TIME);;
    }

    public String getSSID() {
        return SSID.get();
    }

    public void setSSID(String SSID) {
        this.SSID = new SimpleStringProperty(SSID);;
    }

    @Override
    public String toString() {
        return getCLIENT_MAC() + " " + getBSSID() + " " + getTIME() + " " +getSSID();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(obj == this) return true;
        if(!(obj instanceof PacketData)) return false;
        PacketData data = (PacketData) obj;

        return (this.getCLIENT_MAC().equals(data.getCLIENT_MAC())
                && this.getBSSID().equals(data.getBSSID())
                && this.getSSID().equals(data.getSSID()));
    }

    @Override
    public int hashCode()
    {
        return this.SSID.hashCode() + this.CLIENT_MAC.hashCode() + this.BSSID.hashCode();
    }
}
