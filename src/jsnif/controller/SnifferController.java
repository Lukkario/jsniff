package jsnif.controller;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import jsnif.model.SnifferThreadModel;
import jsnif.model.MainModel;
import jsnif.view.ErrorView;
import jsnif.view.MainWindowView;
import jsnif.view.SelectNICView;
import org.pcap4j.core.PcapNativeException;

import java.io.IOException;

public class SnifferController {
    private final MainModel model = new MainModel();
    private SnifferThreadModel snifferThreadModel;
    private Thread snifferThread;
    private String NIC;
    private String errorMessage;

    private MainWindowView mainWindowView;

    public SnifferController()
    {
        MainWindowView.setData(model.getPackets());
        MainWindowView.setController(this);
        SelectNICView.setController(this);
        ErrorView.setController(this);
    }

    public void setNIC(String nic)
    {
        NIC = nic;
    }

    public String getNIC()
    {
        return NIC;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public void setMainWindowView(MainWindowView mainWindowView) {
        this.mainWindowView = mainWindowView;
    }

    public void start() {
        if(setUpNetworkInterface(NIC));
        {
            try {
                openHandle();
                startSnifferThread();
            } catch (PcapNativeException e) {
                setErrorMessage("An error ocured while opening handler. Make sure that provided NIC is correct and set in monitor mode.");
                showErrorView();
            }
            catch (NullPointerException e)
            {
                setErrorMessage("An error ocured while opening handler. Make sure that provided NIC is correct.");
                showErrorView();
            }

        }
    }

    public Boolean setUpNetworkInterface(String nic)
    {
        return model.setUpNetworkInterface(nic);
    }

    private void openHandle() throws PcapNativeException {
        model.openHandle();
    }

    public void startSnifferThread()
    {
        snifferThreadModel = new SnifferThreadModel(MainWindowView.getData(), model.getHandle(), this);
        snifferThread = new Thread(snifferThreadModel);
        snifferThread.start();
    }

    public void stopSnifferThread()
    {
        try {
            snifferThreadModel.stopThread();
            snifferThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        catch (NullPointerException ignored){}
    }

    public void showNicView()
    {
        final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../res/SelectNICWindow.fxml"));
            dialog.setTitle("Select NIC");
            dialog.setScene(new Scene(root, 212, 28));
            dialog.show();
            dialog.setMaxWidth(28);
            dialog.setMaxHeight(212);
            dialog.setMinHeight(28);
            dialog.setMinWidth(212);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showErrorView()
    {
        final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../res/ErrorView.fxml"));
            dialog.setTitle("Error");
            dialog.setScene(new Scene(root, 340, 128    ));
            dialog.show();
            dialog.setMaxWidth(128);
            dialog.setMaxHeight(340);
            dialog.setMinHeight(128);
            dialog.setMinWidth(340);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void enableStart()
    {
        mainWindowView.enableStart();
    }

    public void restartSniffingThread()
    {
        try {
            openHandle();
//            stopSnifferThread();
            startSnifferThread();
        } catch (PcapNativeException e) {
            e.printStackTrace();
        }

    }
}
